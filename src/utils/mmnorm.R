mmnorm <- function (data, minval = 0, maxval = 1) {
  # This is a function to apply min-max normalization to a matrix or dataframe.
  # Min-max normalization subtracts the minimum of an attribute from each value
  # of the attribute and then divides the difference by the range of the attribute.
  # These new values are multiplied by the given range of the attribute
  # and finally added to the given minimum value of the attribute.
  # These operations transform the data into [minval, mxval].
  # Usually minval = 0 and maxval = 1.
  # Uses the scale function found in the R base package.
  # Input: data = The matrix or dataframe to be scaled
  # Output: (nedata, min, range, newmin, newrange)
  
  setClass(Class="mmnorm",
           representation(
             data = "data.frame",
             minv = "numeric",
             rangev = "numeric"
           )
  )
  
  # store all attributes of the original data
  d = dim(data)
  c = class(data)
  cnames = colnames(data)
  
  # Substract by min and divide by range
  minvect = apply(data, 2, min)
  maxvect = apply(data, 2, max)
  rangevect = maxvect - minvect
  zdata1 = scale(data, center = minvect, scale = rangevect)
  
  # Multiply by new range and add the new min
  newminvect = rep(minval, d[2])
  newmaxvect = rep(maxval, d[2])
  newrangevect = newmaxvect - newminvect
  zdata2 = scale(zdata1, center = FALSE, scale = (1/newrangevect))
  zdata3 = zdata2 + newminvect
  
  if (c == "data.frame") {
    zdata3 = as.data.frame(zdata3)
  }
  colnames(zdata3) = cnames
  
  
  return (new("mmnorm",
             data = zdata3,
             minv = minvect,
             rangev = rangevect))
}
